#!/bin/bash
set -eu
host=$1
usr=$2

docker build -t drumpack .
docker save -o ./drumpack.docimage.tar drumpack
scp ./drumpack.docimage.tar $usr@$host:/home/$usr/docker-images