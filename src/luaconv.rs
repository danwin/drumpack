use actix_lua::{LuaActor, LuaActorBuilder, LuaMessage};
use serde_json;
use std::collections::HashMap;

pub struct SerdeJson {inner: serde_json::Value}
impl SerdeJson {
    pub fn from_value(inner: serde_json::Value) -> Self {
        let inner = inner.clone();
        SerdeJson {
            inner
        }
    }

    pub fn into_inner(&self) -> serde_json::Value {
        self.inner.clone()
    }

    pub fn from_lua(lua: LuaMessage) -> SerdeJson {
        use serde_json::Value;
        use serde_json::Number as SerdeNumber;
        let inner: serde_json::Value = match lua {
            LuaMessage::String(s) => 
                Value::String(s),
            LuaMessage::Integer(i) => 
                Value::Number(SerdeNumber::from(i)),
            LuaMessage::Number(float) => 
                Value::Number(SerdeNumber::from_f64(float).unwrap()),
            LuaMessage::Boolean(b) => 
                Value::Bool(b),
            LuaMessage::Nil => 
                Value::Null,
            // HashMap<String, LuaMessage>
            LuaMessage::Table(t) => { 
                let inner = t.into_iter().map(|(k, lua)|{
                    (k, SerdeJson::from_lua(lua).into_inner())
                }).collect::<serde_json::Map<String, Value>>(); 
                Value::Object(inner)},
            LuaMessage::ThreadYield(s) => 
                Value::String(s),
        };
        SerdeJson::from_value(inner)
    }

    pub fn into_lua(&self) -> LuaMessage {
        use serde_json::Value as SerdeValue;

        match &self.inner {

            SerdeValue::Null => 
                LuaMessage::Nil,
            SerdeValue::Bool(b) => 
                LuaMessage::Boolean(*b),
            SerdeValue::Number(v) => {
                if v.is_f64() {
                    LuaMessage::Number(v.as_f64().unwrap())
                } else {
                    LuaMessage::Integer(v.as_i64().expect("Error converting to integer - is it unsigned integer?"))
                }
            },
            SerdeValue::String(s) => 
                LuaMessage::String(s.to_string()),
            SerdeValue::Array(vec) => {
                let stub = "[ARRAY]".to_string();
                LuaMessage::String(stub)
            },
            // Map<String, Value>
            SerdeValue::Object(m) => {
                let t:HashMap<String, LuaMessage> = m.into_iter().map(|(k,v)|{
                    let value = (*v).clone();
                    (k.to_string(), SerdeJson::from_value(value).into_lua())
                }).collect();
                LuaMessage::Table(t)
            }
        }
    }

}


#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn into_lua() {
        let obj = json!({ "a": { "nested": true }, "b": ["an", "array"] });
        let lua = SerdeJson::from_value(obj).into_lua();
        if let LuaMessage::Table(t) = lua {
            let attr_a = t.get("a");
            let attr_nested = attr_a.get("nested");
            assert_eq!(attr_nested, true);
        } else {
            panic!("Lua message invalid conversion")
        }
    }


    fn from_lua() {
        let mut hash = HashMap::new();
        hash.insert("attr", LuaMessage::Integer(1));
        let lua = LuaMessage::Table(hash);
        let result = SerdeJson::from_lua(lua);
        // let obj = json!({ "a": { "nested": true }, "b": ["an", "array"] });
        // let lua = SerdeJson::from_value(obj).into_lua();
        if let Value::Object(obj) = result.into_inner() {
            let attr_a = obj.get("attr");
            let attr_nested = attr_a.get("nested");
            assert_eq!(attr_nested, true);
        } else {
            panic!("SerdeJson message invalid conversion")
        }
    }

}
