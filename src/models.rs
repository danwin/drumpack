use std::collections::HashMap;
use std::convert::From;
use serde::{Serialize, Deserialize};
use serde_json::Value as JsonValue;

/// Mediatior node
#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(tag = "kind", content = "args")]
pub enum RedirectorModel {
    // #[serde(rename = "none")]
    // Null,
    // #[serde(rename = "static")]
    // Static(String),
    // TO-DO: regular rxpression substitution with optional several values from list
    // #[serde(rename = "subst")]
    // Subst(String),
    #[serde(rename = "lua")]
    Lua(String),
    // pub lua_script: String,
}

// impl std::default::Default for RedirectorModel {
//     fn default() -> RedirectorModel {
//         RedirectorModel::Null
//     }
// }



// type MimeType = String;

// #[derive(Clone, Serialize, Deserialize, Debug)]
// pub enum ResponderModel {
//     #[serde(rename = "http")]
//     Http(HashMap<MimeType, HttpResponse>),
//     #[serde(rename = "mqtt")]
//     Mqtt,
// }

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct ResponderModel {
    pub content: String,
    #[serde(default = "default_content_type")]
    pub content_type: String,
}


// /// Reponse conf. TO-DO: Here is http response. Sub-divide also mqtt, etc., using enum.
// #[derive(Clone, Serialize, Deserialize, Debug)]
// pub struct HttpResponse {
//     pub content: String,
//     #[serde(default = "default_http_code")]
//     pub http_code: usize,
// }

// impl std::default::Default for HttpResponse {
//     fn default() -> HttpResponse {
//         HttpResponse {
//             content: String::from("Ok"),
//             // content_type: default_content_type(),
//             http_code: default_http_code(),
//         }
//     }
// }


// /// Mediatior node
// #[derive(Clone, Serialize, Deserialize, Debug)]
// pub struct ResponderModel {
//     pub default_response: HttpResponse,
//     // pub lua_script: String,
// }

/// Generic type of routes vector (Data or Mediator)
#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(tag = "kind", content = "args")]
pub enum LinkConf {
    #[serde(rename = "redirector")]
    Redirector(RedirectorModel),
    #[serde(rename = "responder")]
    Responder(ResponderModel),
}


fn default_http_code() -> usize {200}
fn default_content_type() -> String {"text/html".to_string()}


pub type AttrMap = HashMap<String, JsonValue>;

// #[derive(Clone, Debug, Serialize, Deserialize)]
// pub enum ItemValue {
//     #[serde(rename = "map")]
//     Map(AttrMap),
//     #[serde(rename = "vec")]
//     Vec(Vec<String>),
//     #[serde(rename = "str")]
//     Str(String)
// }

// impl From<Value> for ItemValue {
//     fn from (v: Value) -> ItemValue {
//         use Value::*;
//         match v {
//             Null,
//             Bool(bool),
//             Number(Number),
//             String(String),
//             Array(Vec<Value>),
//             Object(Map<String, Value>),
//         }
//     }
// }