use crate::messages;
use crate::models::{LinkConf};
use crate::handlers::*;
// use crate::luaconv;
// use crate::errors::{MessageError};
use std::convert::{From};
use std::collections::HashMap;
use uuid::Uuid;
use actix::prelude::*;
// use actix::{Actor, Recipient, Arbiter, Context, Handler};
// use actix_broker::{BrokerSubscribe, BrokerIssue, SystemBroker};
use actix_broker::{BrokerSubscribe, BrokerIssue, SystemBroker, ArbiterBroker};
use futures::{future, Future};

// use actix_lua::{LuaActor, LuaActorBuilder, LuaMessage};

// Broadcast messages
/// Notification about create, update (broadcast)
#[derive(Clone, actix::Message)]
pub struct RouteChanged {
    pub sender: Option<Uuid>,
    pub route: String,
    pub conf: LinkConf
}

// impl Message for RouteChanged {
//     type Result = ();
// }

/// Notification about Dropped route (broadcast)
#[derive(Clone, actix::Message)]
pub struct RouteDropped {
    pub sender: Option<Uuid>,
    pub route: String,
}

// impl Message for RouteDropped {
//     type Result = ();
// }

// #[derive(Default)]
pub struct RoutesRegistry {
    routes: HashMap<String, LinkConf>
}

impl std::default::Default for RoutesRegistry {
    fn default() -> RoutesRegistry {
        RoutesRegistry {
            routes: hashmap! {}
        }
    }
}

impl Actor for RoutesRegistry {
    type Context = Context<Self>;
    fn started(&mut self, ctx: &mut Self::Context) {

        // Increase mailbox capacity because this is a sigle instance!
        ctx.set_mailbox_capacity(128);

        // These are broadcast messages to all instances of Router:
        println!("RoutesRegistry started");

        // self.subscribe_system_async::<messages::GetRoutesSnapshot>(ctx);

        self.subscribe_system_async::<RouteChanged>(ctx);
        self.subscribe_system_async::<RouteDropped>(ctx);

    }
}


impl actix::Supervised for RoutesRegistry {}

impl SystemService for RoutesRegistry {
    fn service_started(&mut self, ctx: &mut Context<Self>) {
        println!("RoutesRegistry started");
    }
}

impl Handler<messages::Load> for RoutesRegistry {
    type Result = Result<(), ()>;

    fn handle(&mut self, msg: messages::Load, _ctx: &mut Self::Context) -> Self::Result {
        println!("Registry loaded");
        Ok(())
    }

}

impl Handler<messages::GetRoutesSnapshot> for RoutesRegistry {
    type Result = ();

    fn handle(&mut self, msg: messages::GetRoutesSnapshot, _ctx: &mut Self::Context) {
        let recipient = msg.reply_to;
        let routes = self.routes.clone();
        let snapshot = messages::RoutesSnapshot {routes};
        println!("RoutesRegistry got GetRoutesSnapshot from {:?}", msg.issuer);
        recipient.try_send(snapshot).expect("Error sending RoutesSnapshot message from RoutesRegistry to Router!");
    }

}

impl Handler<RouteChanged> for RoutesRegistry {
    type Result = ();

    fn handle(&mut self, msg: RouteChanged, _ctx: &mut Self::Context) {
        match msg.sender {
            Some(_) => {
                let route = msg.route.clone();
                self.routes.insert(
                    String::from(msg.route), 
                    msg.conf.clone()
                );
                println!("BC: RoutesRegistry changed route {}, {:?}", route, msg.conf);

                // Send donwlink message
                let conf = msg.conf.clone();
                self.issue_system_async(RouteChanged {
                    sender: None,
                    route,
                    conf,
                });
            },
            None => ()
        }
    }
}

impl Handler<RouteDropped> for RoutesRegistry {
    type Result = ();

    fn handle(&mut self, msg: RouteDropped, _ctx: &mut Self::Context) {
        match msg.sender {
            Some(_) => {
                self.routes.remove(&msg.route);
                println!("BC: RoutesRegistry dropped route {}", msg.route);

                // Send donwlink message
                let route = msg.route.clone();
                self.issue_system_async(RouteDropped {
                    sender: None,
                    route,
                });

            },
            None => ()
        }
    }
}



pub struct Router {
    // routes: Vec,
    // pub routes: HashMap<String, LinkConf>,
    pub routes: HashMap<String, LinkActor>,
    pub instance_id: Uuid,
    pub is_loaded: bool,

    // pub lua_reducer: LuaActor,
    // pub lua_script: Option<String>,

    // In this level send and send bulk similar. SenBulk can also have pre-processing 
}

impl Router {
    pub fn new() -> Router {
        Router {
            .. Default::default()
        }
    }

    fn set_route(&mut self, route: &str, conf: &LinkConf) {
        self.routes.insert(
            String::from(route), 
            LinkActor::from(conf)
        );
    }

    fn drop_route(&mut self, route: &str) {
        self.routes.remove(route);
    }

}

impl Actor for Router {

    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        // These are broadcast messages to all instances of Router:
        ctx.set_mailbox_capacity(256);

        println!("Router {:?} started", self.instance_id.clone());

        // Aquire initial config:
        let snapshot = messages::GetRoutesSnapshot{ 
            reply_to: ctx.address().recipient(),
            issuer: self.instance_id.clone()
        };
        RoutesRegistry::from_registry().try_send(snapshot).expect("Error sending GetRoutesSnapshot from Router to RoutesRegistry!"); // Request to SystemService
        
        self.subscribe_system_async::<RouteChanged>(ctx);
        self.subscribe_system_async::<RouteDropped>(ctx);

    }
}


impl Default for Router {
    fn default() -> Router {
        Router {
            routes: hashmap! {},
            instance_id: Uuid::new_v4(),
            is_loaded: false,

            // lua_reducer: LuaActorBuilder::new(),
            // lua_script: None
        }
    }
}

impl actix::Supervised for Router {}

impl ArbiterService for Router {}

// impl actix::Supervised for Router {}

// impl ArbiterService for Router {
//     fn service_started(&mut self, ctx: &mut Context<Self>) {
//         println!("Router Service started, routes: {:?}", self.routes.keys().map(|k| format!("{}",k)).collect::<Vec<String>>());
//     }
// }

impl Handler<messages::RoutesSnapshot> for Router {
    type Result = ();

    fn handle(&mut self, msg: messages::RoutesSnapshot, _ctx: &mut Self::Context) {

        for (route, conf) in msg.routes.iter() {
            self.set_route(route, conf)
        }

        println!("BC: Router {} - applying routes snapshot", self.instance_id.clone());
    }

}

impl Handler<RouteChanged> for Router {
    type Result = ();

    fn handle(&mut self, msg: RouteChanged, _ctx: &mut Self::Context) {
        match msg.sender {
            None => {
                self.set_route(&msg.route, &msg.conf);
                println!("BC: Instance {} changed route {}, {:?}", self.instance_id.clone(), msg.route, msg.conf);
            },
            _ => ()
        }
    }
}

impl Handler<RouteDropped> for Router {
    type Result = ();

    fn handle(&mut self, msg: RouteDropped, _ctx: &mut Self::Context) {
        match msg.sender {
            None => {
                self.drop_route(&msg.route);
                println!("BC: Instance {} dropped route {}", self.instance_id.clone(), msg.route);
            },
            _ => ()
        }
    }
}


impl Handler<messages::GetRoute> for Router {
    type Result = Option<messages::GetRouteResponse>;

    fn handle(&mut self, msg: messages::GetRoute, _ctx: &mut Self::Context) -> Option<messages::GetRouteResponse> {
        // println!("[{}] Getting route: {:?}", self.instance_id, msg);
        let route = msg.route;
        if self.routes.contains_key(&route) {
            let conf =  LinkConf::from(self.routes.get(&route).unwrap());
            let item = messages::GetRouteResponse {
                route: route,
                conf
            };
            Some(item)
        } else {
            None
        }
    }
}


impl Handler<messages::CreateRoute> for Router {
    type Result = bool;

    fn handle(&mut self, msg: messages::CreateRoute, ctx: &mut Self::Context) -> bool {
        println!("[{}] Creating route: {:?}", self.instance_id, msg);
        let route = msg.route;
        let conf = msg.conf;
        // let force: bool = match msg.force {
        //     Some(b) => b,
        //     None => false
        // };
        // if force || ! self.routes.contains_key(&route) {
        //     let conf = msg.conf;
        //     self.issue_system_async(RouteChanged {
        //         sender: self.instance_id.clone(),
        //         route,
        //         conf,
        //     });
        //     true
        // } else {
        //     false
        // }
        self.issue_system_async(RouteChanged {
            sender: Some(self.instance_id.clone()),
            route,
            conf,
        });
        true
    }
}

impl Handler<messages::UpdateRoute> for Router {
    type Result = bool;

    fn handle(&mut self, msg: messages::UpdateRoute, ctx: &mut Self::Context) -> bool {
        println!("[{}] Updating route: {:?}", self.instance_id, msg);
        let route = msg.route;
        let conf = msg.conf;
        // let route = msg.route.clone();
        // if self.routes.contains_key(&route) {
        //     let conf = msg.conf;
        //     self.issue_system_async(RouteChanged {
        //         sender: self.instance_id.clone(),
        //         route,
        //         conf,
        //     });
        //     true
        // } else {
        //     false
        // }
        self.issue_system_async(RouteChanged {
            sender: Some(self.instance_id.clone()),
            route,
            conf,
        });
        true
    }
}

impl Handler<messages::DropRoute> for Router {
    type Result = bool;

    fn handle(&mut self, msg: messages::DropRoute, ctx: &mut Self::Context) -> bool {
        println!("[{}] Dropping route: {:?}", self.instance_id, msg);
        let route = msg.route;
        if self.routes.contains_key(&route) {
            self.issue_system_async(RouteDropped {
                sender: Some(self.instance_id.clone()),
                route,
            });
            true
        } else {
            false
        }
    }
}


impl Handler<messages::EnumRoutes> for Router {
    type Result = Option<messages::EnumRoutesResponse>;

    fn handle(&mut self, msg: messages::EnumRoutes, _ctx: &mut Self::Context) -> Option<messages::EnumRoutesResponse> {
        // self.routes.keys().collect::Vec<String>()
        println!("[{}] Enumerating routes: {:?}", self.instance_id, msg);
        let mut result = messages::EnumRoutesResponse{items: Vec::new()};
        for (route, link) in self.routes.iter() {
            let conf =  LinkConf::from(link);
            // println!("route: {}, conf: {:?}", route, conf);
            let rec = messages::GetRouteResponse{
                route: route.to_string(),
                conf: conf.clone()
            };
            result.items.push(rec);
        }
        Some(result)
    }
}


// impl Handler<messages::Event> for Router {
//     type Result = Option<messages::EventResponse>;

//     fn handle(&mut self, msg: messages::Event, _ctx: &mut Self::Context) -> Option<messages::EventResponse> {
//         let route_val = msg.ctx.get("route".to_string());
//         match route_val {
//             messages::ItemValue::Str(route) => {},
//             _ => None
//         }
//         if let messages::ItemValue::Str(route) = route_val {
//             match self.get(route) {
//                 Some(link_actor) => {},
//                 None => None
//             }
//         }
//         // self.routes.keys().collect::Vec<String>()
//         println!("[{}] Enumerating routes: {:?}", self.instance_id, msg);
//         let mut result = messages::EnumRoutesResponse{items: Vec::new()};
//         for (route, link) in self.routes.iter() {
//             let conf =  LinkConf::from(*link);
//             println!("route: {}, conf: {:?}", route, conf);
//             let rec = messages::GetRouteResponse{
//                 route: route.to_string(),
//                 conf: conf.clone()
//             };
//             result.items.push(rec);
//         }
//         Some(result)
//     }
// }

/// Add or update route (direct)
#[derive(Clone, Debug)]
pub struct GetRouteLink {
    pub route: String, 
}

impl From<String> for GetRouteLink {
    fn from(route: String) -> Self {
        GetRouteLink {route: route.clone()}
    }
}

/// Response for GetRoute
impl Message for GetRouteLink {
    type Result = Option<Recipient<messages::Event>>;
}


impl Handler<GetRouteLink> for Router {
    type Result = Option<Recipient<messages::Event>>;

    fn handle(&mut self, msg: GetRouteLink, _ctx: &mut Self::Context) -> Option<Recipient<messages::Event>> {
        let route = msg.route.clone();
        let route_link = self.routes.get(&route);
        match route_link {
            Some(ref link) => Some(link.get_recipient().clone()),
            _ => None
        }
    }
}

// impl Handler<messages::SyncRoutesWith> for Router {
//     type Result = ();

//     fn handle(&mut self, msg: messages::SyncRoutesWith, ctx: &mut Self::Context) {
//         let source_addr = msg.addr;
//         if !self.is_loaded {
//             println!("SyncRoutesWith - Running sync!");
//             source_addr.send(messages::EnumRoutes::default()).wait().and_then(|routes| {
//                 println!("===> {:?}", routes);
//                 self.is_loaded = true;
//                 Ok(())
//             });
//             //.wait(ctx)
//         } else {
//             println!("SyncRoutesWith - No sync necessary!")
//         }
//     }

// }


// impl From<messages::HandleRequest> for LuaMessage {
//     fn from(msg: messages::HandleRequest) -> LuaMessage {
//         let query: HashMap<String, LuaMessage> = msg.query.iter().map(|(k, v)| (k, LuaMessage.from(v))).collect();
//         LuaMessage::from(
//             !hashmap {
//                 "path" =>   LuaMessage::from(msg.path.clone()),
//                 "query" =>  LuaMessage::from(query),
//                 // headers: HashMap<String,Vec<String>>,
//                 // cookies:   HashMap<String,String>,
//             }
//         )
//     }
// }

// impl Handler<messages::HandleRequest> for Router {
//     type Result = Result<(), MessageError>;

//     fn handle(&mut self, msg: messages::HandleRequest, ctx: &mut Context<Self>) ->  Result<(), MessageError> {
//         // Reduce route by lua mediator:
//         let route;
//         match self.lua_reducer.
//         if let Some(ref route_item) = self.routes.get(&msg.route) {
//             Ok((route_item.clone()))
//         } else {
//             Err(MessageError::RouteNotFound(msg.route))
//         }

//     }
// }


// /// Main representation of the input request
// pub struct Event {
//     // Keep status for processing if necessary:
//     pub ctx:    HashMap<String, String>,
//     // Put path, header, cookies, query here? 
//     pub payload: HashMap<String, String>,
// }

// pub struct EventResponse {
//     pub redirect: Option<String>,
//     pub response: Option<models::DataEntry>,
//     pub event: Option<Event>,
// }


/// Wrapper for the route actor. "Final"
pub struct LinkActor {
    inner: Recipient<messages::Event>,
    /// Saved config for API quick response 
    conf: LinkConf
}

/// Target element of a route. 
/// Container with actor and its conf
impl LinkActor {
    /// Return recipient for Event message
    fn get_recipient(&self) -> Recipient<messages::Event> {
        self.inner.clone()
    }
}


impl From<&LinkConf> for LinkActor {

    fn from(conf: &LinkConf) -> Self {
        use LinkConf::*;
        let conf = conf.clone();
        let inner = match conf {
            Redirector(ref c) => RedirectorActor::from(c).start().recipient::<messages::Event>(),
            Responder(ref c) => ResponderActor::from(c).start().recipient::<messages::Event>(),
            _ => panic!("Unknown model of LinkConf"),
        };
        Self {
            inner,
            conf
        }
    }
}

impl From<&LinkActor> for LinkConf {

    fn from(link: &LinkActor) -> Self {
        link.conf.clone()
    }
}

// mod drivers {
//     use actix::prelude::*;
//     use actix_lua::{LuaActor, LuaActorBuilder, LuaMessage};
//     use crate::messages::{Event, EventResponse, AttrMap};
//     use crate::luaconv;

//     pub struct Lua {
//         pub inner: Addr<LuaActor>,
//     }

//     impl Lua {
//         fn from_source(script: String) -> Self {
//             let inner: Addr<LuaActor> = LuaActorBuilder::new()
//                 .on_handle_with_lua(&script)
//                 .build()
//                 .unwrap().start();
//             Lua {
//                 inner
//             }
//         }
//     }

//     impl Actor for Lua {
//         type Context = Context<Self>;
//     }

//     impl Handler<Event> for Lua {

//         type Result = Option<EventResponse>;

//         fn handle(&mut self, msg: Event, _ctx: &mut Self::Context) -> Option<EventResponse> {

//         }

//     }

//     impl From<AttrMap> for LuaMessage {
//         fn from(am: AttrMap) -> Self {
//             luaconv::parse_hash(am)
//         }

//     }

//     impl From<Event> for LuaMessage {

//         fn from(e: Event) -> Self {
//             use messages::ItemValue;

//             let table: HashMap<String, LuaMessage> = HashMap::new();

//             table.insert("ctx".to_string(), LuaMessage::from(e.ctx));
//             table.insert("conf".to_string(), LuaMessage::from(e.conf));

//             LuaMessage::from(table)

//         }

//     }

//     // impl From<LuaMessage> for messages::EventResponse {
//     //     fn from(msg: LuaMessage) -> messages::EventResponse {

//     //         use messages::{Event, EventResponse, ItemValue, AttrMap};
            
//     //         match msg {
//     //             let event = 
//     //             LuaMessage::Table(hashmap) => EventResponse {
//     //                 redirect: try_string_option(get_attr(msg, "redirect")),
//     //                 // response: try_option_dataentry(get_attr(msg, "response")),
//     //                 response: None, /// <-- Use Lua for routing, not for response.
//     //                 event: try_event_struct_option(lm: LuaMessage)(get_attr(msg, "event"))
//     //             },
//     //             _ => panic!("")
//     //         }
//     //     }
//     // }

//     // fn build_lua_message(route: String, query: HashMap<String, String>, headers: HeadersUniqMap) -> LuaMessage {
//     //     let query: HashMap<String, LuaMessage> = query.iter().map(|(k, v)| (k.to_string(), LuaMessage::from(v.to_string()))).collect();
//     //     let headers: HashMap<String, LuaMessage> = headers.iter().map(|(k, v)| (k.to_string(), LuaMessage::from(v.to_string()))).collect();
//     //     let table: HashMap<String, LuaMessage> = HashMap::new();

//     //     table.insert("path".to_string(), LuaMessage::from(route));
//     //     table.insert("query".to_string(), LuaMessage::from(query));
//     //     table.insert("headers".to_string(), LuaMessage::from(headers));

//     //     LuaMessage::from(table)
//     // }

//     // let lua = LuaActorBuilder::new()
//     //     .on_handle_with_lua(&script)
//     //     .build()
//     //     .unwrap().start();


// }