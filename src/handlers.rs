use actix::prelude::*;
use actix_lua::{LuaActor, LuaActorBuilder, LuaMessage};
use actix::Actor;
use serde_json;

use std::convert::From;

use crate::messages::{Event, EventResponse, Directive, EventResultType};
use crate::models::{ResponderModel, RedirectorModel};
use crate::luaconv;
use crate::errors::{SvcError};

use futures::future::{ok as fut_ok, err as fut_err, Future, Either, IntoFuture};

// #[derive(Default)]
pub struct RedirectorActor {
    inner: RedirectorModel,
    lua_actor: Option<Addr<LuaActor>>
}

static LSCRIPT: &str = r#"
function serializeTable(val, name, skipnewlines, depth)
    skipnewlines = skipnewlines or false
    depth = depth or 0

    local tmp = string.rep(" ", depth)

    if name then tmp = tmp .. name .. " = " end

    if type(val) == "table" then
        tmp = tmp .. "{" .. (not skipnewlines and "\n" or "")

        for k, v in pairs(val) do
            tmp =  tmp .. serializeTable(v, k, skipnewlines, depth + 1) .. "," .. (not skipnewlines and "\n" or "")
        end

        tmp = tmp .. string.rep(" ", depth) .. "}"
    elseif type(val) == "number" then
        tmp = tmp .. tostring(val)
    elseif type(val) == "string" then
        tmp = tmp .. string.format("%q", val)
    elseif type(val) == "boolean" then
        tmp = tmp .. (val and "true" or "false")
    else
        tmp = tmp .. "\"[inserializeable datatype:" .. type(val) .. "]\""
    end

    return tmp
end

-- MAIN:
print("received " .. serializeTable(ctx.msg, "ctx.msg"))
--return {directive="redirect", event=nil}
return {directive={kind="redirect", args="b/name_a"}}
"#;


/// Sample table inside Lua mediator looks like: 
/// ctx.msg = {
//  ctx = {
//   headers = {
//    sec-fetch-mode = "cors",
//    accept-language = "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
//    sec-fetch-site = "cross-site",
//    accept = "*/*",
//    cookie = "_ga=GA1.1.903892399.1562753355; af_lpdid=3%3A1111819953; GED_PLAYLIST_ACTIVITY=W3sidSI6Ijl2d0YiLCJ0c2wiOjE1NjY1NTc2OTksIm52IjoxLCJ1cHQiOjE1NjY1NTUwMTEsImx0IjoxNTY2NTU3Njk4fV0.; c87686a82fc35a3078911e55fbf7b129=pcfceo4cf1f0jfi6tb8bvevbqi; d02d362dfc4fef4f9e72450fe4ece387=s1om94e12l1omc9h55ivt33si8",
//    host = "localhost:8010",
//    connection = "keep-alive",
//    user-agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36",
//    accept-encoding = "gzip, deflate, br",
//   },
//   query = {
//   },
//   route = "name_lua",
//  },
//  payload = {
//  },
// }


/// For redirection, make: return {directive={kind="redirect", args="b/name_a"}}
/// // Note that it is possible to omit event field, this means that original event remains unchanged


impl From<&RedirectorModel> for RedirectorActor {
    fn from(inner: &RedirectorModel) -> Self {
        let inner = inner.clone();
        match inner.clone() {
            RedirectorModel::Lua(script) => {
                // use actix::*; // <--- fixing errors like "no method named `start` found for type `actix_lua::actor::LuaActor` in the current scope"
                let lua_actor = LuaActorBuilder::new()
                    .on_handle_with_lua(&script)
                    // .on_handle_with_lua(LSCRIPT)
                    .build();
                let lua_actor = match lua_actor {
                    Ok(a) => Some(a.start()),
                    Err(e) => {
                        error!("Error in script: {}", e);
                        None
                    }
                };
                Self {
                    inner,
                    lua_actor,
                }                
            },
        }
    }
}

impl Actor for RedirectorActor {
    type Context = Context<Self>;

    // fn started(&mut self, ctx: &mut Self::Context) {
    //     if let RedirectorModel::Lua(script) = self.inner {
    //         // use actix::*; // <--- fixing errors like "no method named `start` found for type `actix_lua::actor::LuaActor` in the current scope"
    //         self.lua_actor = LuaActorBuilder::new()
    //             .on_handle_with_lua(&script)
    //             .build()
    //             .expect("Cannot build Lua Actor")
    //             .start();
    //     }
    // }

}


impl Handler<Event> for RedirectorActor {

    type Result = ResponseActFuture<Self, EventResponse, SvcError>;

    fn handle(&mut self, msg: Event, _ctx: &mut Self::Context) -> Self::Result {
        use serde_json::error::Error as SerdeJsonError;

        let msg2 = msg.clone();

        let f = match self.inner.clone() {
            // RedirectorModel::Null => Ok(None).into_future(),
            // RedirectorModel::Static(to) => {
            //     let event_response = EventResponse {
            //         directive: Directive::redirect(to),
            //         event: Some(msg.clone()),
            //     };
            //     let f = Ok(event_response).into_future();
            //     let case_a = actix::fut::wrap_future::<_, Self>(f);

            //     Either::A(case_a)
            // },
            // Subst(String),
            RedirectorModel::Lua(_) => {
                let json_val = serde_json::to_value(msg).unwrap();
                let lua_msg = luaconv::SerdeJson::from_value(json_val).into_lua();
                if let None = self.lua_actor.as_ref() {
                    // let f = fut_err(SvcError::LuaError("Syntax error".to_string()));
                    let f = Err(SvcError::LuaError("Syntax error".to_string())).into_future();
                    let case_a = actix::fut::wrap_future::<_, Self>(f);
                    // let case_a = futures::done(SvcError::LuaError("Syntax error".to_string()));
                    return Box::new(case_a);
                }
                let chain = self.lua_actor.as_ref().expect("Lua actor was not started!").send(lua_msg)
                    // .from_err()
                    .map_err(SvcError::from)
                    .and_then(|lua_resp: LuaMessage| {
                        match lua_resp {
                            LuaMessage::String(new_route) => Ok(new_route),
                            _ => Err(SvcError::SerdeJson),
                        }
                    })
                    // .map_err(SvcError::from)
                    ;

                let case_b = actix::fut::wrap_future::<_, Self>(chain);
                // let mapped = f.map(|lua_resp, actor, _ctx| {
                //     let value: serde_json::Value = luaconv::SerdeJson::from_lua(lua_resp);
                //     let resp: EventResponse = serde_json::from_value(value.into_inner());
                //     resp
                // });
                // Either::A(wf)
                case_b.map(|new_route, _actor, _ctx| {
                    println!("LUA RETURNS: {:?}", new_route);
                    EventResponse{
                        directive: Directive::Redirect(new_route),
                        event: Some(msg2)
                    }
                    // match result.event {
                    //     None => EventResponse{
                    //         directive: result.directive,
                    //         event: Some(msg2)
                    //     },
                    //     Some(_) => result
                    // }
                })
                // // let case_b = actix::fut::wrap_future::<_, Self>(case_b);
                // // case_b
                // case_b
                // chain
            }
        };
        Box::new(f)
        // let directive = Directive::Redirect("/next".to_string());
        // let event = Some(msg.clone());
        // Some(EventResponse {
        //     directive,
        //     event,
        // })
    }

}

pub struct ResponderActor {
    inner: ResponderModel,
}

impl From<&ResponderModel> for ResponderActor {
    fn from(inner: &ResponderModel) -> Self {
        let inner = inner.clone();
        Self {
            inner
        }
    }
}

impl Actor for ResponderActor {
    type Context = Context<Self>;
}

impl Handler<Event> for ResponderActor {

    type Result = ResponseActFuture<Self, EventResponse, SvcError>;

    // type Result = EventResultType;

    fn handle(&mut self, msg: Event, _ctx: &mut Self::Context) -> Self::Result {
        let directive = Directive::Reply(self.inner.clone());
        let event = Some(msg.clone());
        let f = Ok(EventResponse {
            directive,
            event,
        }).into_future();
        let wrapped = actix::fut::wrap_future::<_, Self>(f);
        // let wrapped = wrapped.map(|result, actor, _ctx| {
        //     result
        // });
        Box::new(wrapped)
    }

}

