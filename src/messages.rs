/// Backbone messages
use actix::prelude::*;
// use actix::prelude::Message;
// use actix_broker::{BrokerSubscribe, BrokerIssue};
// use crate::errors::{MessageError};
// use crate::payload::{Payload};
use crate::models;
use crate::errors;

use std::collections::HashMap;
use std::convert::{From};
use serde::{Serialize, Deserialize};
use serde_json::Value as JsonValue;
// use serde::ser::{Serialize, Serializer};

// NEW MESSAGES:
use uuid::Uuid;
use std::default;
// use actix::prelude::*;



// pub struct HttpRequestMessage {

// }


/// Main representation of the input request
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Event {
    // Keep status for processing if necessary:
    pub ctx: models::AttrMap,
    // Put path, header, cookies, query here? 
    pub payload: models::AttrMap,
}

impl Event {
    pub fn get_route(&self) -> Result<String, &'static str> {
        match self.ctx.get("route") {
            Some(item) => {
                match item {
                    JsonValue::String(r) => Ok(r.clone()),
                    _ => Err("Invalid value type of route"),
                }
            },
            _ => Err("Invalid value of route")
        }
    }

    pub fn set_route(&mut self, route: String) {
        self.ctx.insert("route".to_string(), JsonValue::String(route));
    }
}

/// Further action for Event
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(tag = "kind", content = "args")]
pub enum Directive {
    #[serde(rename = "redirect")]
    Redirect(String),
    #[serde(rename = "reply")]
    Reply(models::ResponderModel),
}

impl Directive {
    #[inline]
    pub fn redirect(to: String) -> Directive {
        Directive::Redirect(to.to_owned())
    }
    #[inline]
    pub fn reply(content_type: String, content: String) -> Directive {
        Directive::Reply(models::ResponderModel{
            content,
            content_type,
        })
    }
}

/// Result of Event processing
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct EventResponse {
    /// Next action for message
    pub directive: Directive,
    // pub redirect: Option<String>,
    // pub response: Option<models::Response>,
    /// Current valuer of event (after possible transformations)
    /// See serde possibilities: skip_serializing_if = "Option::is_none"
    #[serde(default)]
    pub event: Option<Event>,
}


// Shortcut to Result type of Event:
pub type EventResultType = Result<EventResponse, errors::SvcError>;

/// Response for Event
impl Message for Event {
    // type Result = EventResponse;
    type Result = EventResultType;
    // type Result = ResponseActFuture<Self, EventResultType, errors::SvcError>;
}


// --------- 8< --------------
/// Add or update route (direct)
#[derive(Clone, Deserialize, Debug)]
pub struct GetRoute {
    pub route: String, 
}

impl From<String> for GetRoute {
    fn from(route: String) -> Self {
        GetRoute {route: route.clone()}
    }
}

/// Response for GetRoute
impl Message for GetRoute {
    type Result = Option<GetRouteResponse>;
}

/// Add new route (broadcast)
#[derive(Clone, Deserialize, Debug)]
pub struct CreateRoute {
    pub route: String, 
    pub conf: models::LinkConf,
    pub force: Option<bool>,
}

/// Response for CreateRoute
impl Message for CreateRoute {
    type Result = bool;
}

/// Update route (broadcast)
#[derive(Clone, Deserialize, Debug)]
pub struct UpdateRoute {
    pub route: String, 
    pub conf: models::LinkConf 
}

/// Response for messages::UpdateRoute
impl Message for UpdateRoute {
    type Result = bool;
}

/// Drop route (broadcast)
#[derive(Clone, Deserialize, Debug)]
pub struct DropRoute {
    pub route: String,
}

impl From<String> for DropRoute {
    fn from(route: String) -> Self {
        DropRoute {route: route.clone()}
    }
}

/// Response for DropRoute
impl Message for DropRoute {
    type Result = bool;
}

/// Enum routes (direct)
#[derive(Clone, Serialize, Deserialize, Debug, Default)]
pub struct EnumRoutes {
    pub filter: Option<String>,
}

/// Response for EnumRoutes
impl Message for EnumRoutes {
    type Result = Option<EnumRoutesResponse>;
}

/// Check route (direct)
#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct CheckRoute {
    pub route: String,
}

/// Response for CheckRoute
impl Message for CheckRoute {
    type Result = bool;
}



// // Broadcast messages
// /// Notification about create, update (broadcast)
// #[derive(Clone, Debug, Message)]
// pub struct RouteChanged {
//     pub sender: Uuid,
//     pub route: String,
//     pub conf: models::LinkConf
// }

// // impl Message for RouteChanged {
// //     type Result = ();
// // }

// /// Notification about Dropped route (broadcast)
// #[derive(Clone, Debug, Message)]
// pub struct RouteDropped {
//     pub sender: Uuid,
//     pub route: String,
// }

// // impl Message for RouteDropped {
// //     type Result = ();
// // }


/// Special signal about failure.
#[derive(Clone, Message)]
pub struct Fail {
    message: String
}


// Auxillary types for messages

/// Response for GetRoute
#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct GetRouteResponse {
    pub route: String, 
    pub conf: models::LinkConf
}

/// Response for EnumRoutes
// #[derive(Clone, Serialize, Deserialize, Debug, MessageResponse)]
#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct EnumRoutesResponse {
    pub items: Vec<GetRouteResponse>
}


// Broadcast messages with RoutesRegistry
/// Request to RoutesRegistry for routes snapshot (broadcast)
#[derive(Clone, Message)]
pub struct GetRoutesSnapshot {
    /// Router which sends this message:
    pub reply_to: actix::Recipient<RoutesSnapshot>,
    pub issuer: Uuid,
    // pub route: String,
    // pub conf: models::LinkConf
}

/// Complementary message from RoutesRegistry as a response for GetRoutesSnapshot (narrow, not broadcast)  
#[derive(Clone, Debug, Message)]
pub struct RoutesSnapshot {
    /// Router which sends original request:
    pub routes: HashMap<String, models::LinkConf>,
    // pub route: String,
    // pub conf: models::LinkConf
}

pub struct Load;

impl Message for Load {
    type Result = Result<(), ()>;
}

// // route: &str, payload: &models::LinkConf
// #[derive(Clone, Message)]
// pub struct SyncRoutesWith {
//     pub addr: Recipient<EnumRoutes>
// }
