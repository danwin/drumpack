#[macro_use]
extern crate actix;
extern crate actix_broker;
extern crate actix_lua;
extern crate actix_web;
extern crate futures;
extern crate future_union;
extern crate num_cpus;
use error::Error;

// extern crate fastlog;
#[macro_use]
extern crate log;
extern crate env_logger;
use log::Level;

use futures::future::{Future, IntoFuture, result, ok as fut_ok, err as fut_err, finished, Either, join_all};
// use future_union::future_union;

#[macro_use] extern crate serde;
#[macro_use] extern crate serde_derive; 
#[macro_use] extern crate serde_json;
// #[macro_use] extern crate actix_derive;

extern crate itertools;

use actix_web::dev::Payload; // <--- for dev::Payload
use actix_broker::{BrokerSubscribe, BrokerIssue};
// use actix_redis::{Command, RedisActor, Error as ARError}; 
use actix_web::{
    get, // <- here is "decorator"
    http, 
    error,
    middleware, 
    HttpServer, 
    App, Error as AWError, HttpMessage, ResponseError,
    HttpRequest, HttpResponse,
    FromRequest,
    // FutureResponse, 
    // web::Data, 
    web,
    web::Path, web::Query, web::Json, 
    // web::Payload,
};




// use actix_web::actix_cors::Cors;
use actix_cors::Cors;

use derive_more::Display; // naming it clearly for illustration purposes

// use serde::ser::{Serialize, Serializer, SerializeStruct};
use serde::{Serialize, Serializer};

use serde_json::{Value as JsonValue};

// use failure::Error;

#[macro_use] extern crate maplit;

// use std::thread;
#[macro_use] extern crate failure;
// mod errors;
// use errors::{ServiceError, ConfError};

// use actix_lua::{LuaActor, LuaActorBuilder, LuaMessage};
// use actix::Actor; // <--- fixing errors like "no method named `start` found for type `actix_lua::actor::LuaActor` in the current scope"
use actix::prelude::*;
use actix::{Actor, Addr, Arbiter, Context, Handler, System, MailboxError};

// mod payload;
mod models;
mod messages;
mod errors;
use errors::*;

use messages::{Event, EventResponse, Directive, EventResultType};
use models::{AttrMap, ResponderModel};
mod luaconv;
mod handlers;

mod dpcore;
use dpcore::{Router, RoutesRegistry, GetRouteLink, LinkActor};

// use crate::core::{Router, RoutesRegistry};

use std::collections::HashMap;
use std::iter::IntoIterator;
// mod models;
// mod schema;
// mod conf_api;

// // extern crate base64;

// // static beacon_resp: String = base64::decode("R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7").unwrap().collect().join("");

// type WriterFn = fn(&str, &str) -> std::io::Result<()>;
// mod dumpsync;
// static FILEWRITER:WriterFn = dumpsync::writefile;

extern crate dotenv;
use dotenv::dotenv;
use std::env;

// mod confregistry;
// use confregistry::{Config, ConfProvider, TomlProvider, EnvProvider, PostgresProvider, RouteConf};



type HeadersUniqMap = HashMap<String,String>;
/// Headers extactor
struct Headers {
    inner: HeadersUniqMap,
    // headers: HashMap<String,Vec<String>>
}

impl Headers {
    /// Deconstruct to an inner value
    pub fn into_inner(self) -> HeadersUniqMap {
        self.inner
    }    
}

impl FromRequest for Headers {
    type Config = ();
    // type Result = Result<Self, Error>;

    type Error = Error;
    type Future = Result<Self, Error>;
    // type Config = QueryConfig;

    #[inline]
    fn from_request(req: &HttpRequest, _: &mut Payload) -> Self::Future {
        let req = req.clone();
        let native_headers = req.headers();
        // let mut headers_info: HashMap<String, Vec<String>> = HashMap::new();
        let mut inner:HeadersUniqMap = HashMap::new();

        for key in native_headers.keys() {
            // let vals: Vec<String> = native_headers.get_all(key).iter().map(|h|format!("{:?}", h)).collect(); // <-- header can be non-string with opaque bytes
            let vals: Vec<String> = native_headers.get_all(key).into_iter().map(|h|{
                match h.to_str() {
                    Ok(s) => s.to_string(),
                    _ => "-".to_string(),
                }
            }).collect(); // <-- header can be non-string with opaque bytes
            inner.insert(key.to_string(), vals[0].clone());
        }

        // let inner:HeadersUniqMap = req.headers()
        //     .iter()
        //     .map(|(k, v)|{
        //         (String::from(k.as_str()), format!("{:?}", v))}
        //     ).collect();
        Ok(Headers{ inner })
    }
}


//////////// 




// CONF API

#[derive(Deserialize)]
struct Info {
    route: String,
}

#[derive(Serialize)]
struct OperationResponse {
    success: bool,
    error: Option<String>,
}

impl Default for OperationResponse {
    fn default() -> Self {
        OperationResponse {
            success: true,
            error: None,
        }
    }
}


struct ChainEnv {
    router: Addr<Router>
}


/// Helpers
/// 
/// 
type SerdeHashMap = serde_json::Map<String, serde_json::Value>;

/// Http request -> Event massage
fn encode_event(route: String, query: HashMap<String, String>, headers: HashMap<String, String>) -> Result<Event, Error> {
    let mut payload_msg = Event {
        ctx: HashMap::new(),
        payload: HashMap::new(),
    };
    let attr_query: SerdeHashMap = query.into_iter().map(|(k, v)| (k, JsonValue::String(v))).collect();
    let attr_headers: SerdeHashMap = headers.into_iter().map(|(k, v)| (k, JsonValue::String(v))).collect();

    payload_msg.ctx.insert("route".to_string(), JsonValue::String(route.clone()));
    payload_msg.ctx.insert("query".to_string(), JsonValue::Object(attr_query));
    payload_msg.ctx.insert("headers".to_string(), JsonValue::Object(attr_headers));

    // TO-DO: Add "body" also

    Ok(payload_msg)
}

/// Find route for Event
fn step_route(msg: Event, router: Addr<Router>) -> impl Future<Item = Recipient<Event>, Error = SvcError> {
    let route; 
    match msg.get_route() {
        Ok(value) => {route = value.clone()},
        Err(err) => return Either::A( fut_err(SvcError::NotFound{})),
    };
    Either::B(router
        .send(GetRouteLink::from(route))
        // .map_err(SvcError::from)
        .from_err()
        .and_then(|recipient: Option<Recipient<Event>>| {
            // fut_ok(recipient.unwrap())
            match recipient {
                Some(r) => Either::A(fut_ok(r)),
                None => Either::B(fut_err(SvcError::NotFound {}))
            }
        })
    )
}

fn step_handle_event(msg: Event, link_actor: Recipient<Event>) -> impl Future<Item = EventResponse, Error = SvcError> {
    link_actor
        .send(msg)
        .from_err()
        .and_then(|event_response: EventResultType| {
            match event_response {
                Ok(r) => fut_ok(r),
                // Err(_) => fut_err(SvcError::NotFound {})
                Err(e) => fut_err(e)
            }
        })
}


// REFACTOR THIS PART!
fn step_handle_directive_inner(router: Addr<Router>, event_response: EventResponse) -> impl Future<Item = ResponderModel, Error = SvcError> {
    use Directive::*;
    match event_response.directive {
        Redirect(new_route) => {
            // // Recursive option - not allowed in current version ... 
            // let mut next_event = event_response.event.unwrap().clone();
            // next_event.ctx.insert("route".to_string(), JsonValue::String(new_route));
            // Either::A(Either::A(handle_event(next_event, router.clone())))

            let mut msg = event_response.event.unwrap().clone();
            msg.set_route(new_route);
            let msg2 = msg.clone();
            Either::A(step_route(msg, router).and_then(move |link_actor: Recipient<Event>| {
                step_handle_event(msg2, link_actor).and_then(move |res: EventResponse| {
                    match res.directive {
                        Reply(r) => {
                            // println!("--->step_handle_directive_inner OK {:?}", r.clone());

                            Either::A(fut_ok(r))
                        },
                        _ => {
                            // println!("--->step_handle_directive_inner RECURRENT ROUTE ERROR");
                            Either::B(fut_err(SvcError::RecurrentRouting{}))
                        }
                    }
                })
            }))
        },
        Reply(r) => Either::B(Either::A(fut_ok(r))),
        _ => Either::B(Either::B(fut_err(SvcError::NotFound{})))
    }
}

fn handle_event_inner(msg: Event, router: Addr<Router>) -> impl Future<Item = ResponderModel, Error = SvcError> {
    let msg2 = msg.clone();
    let router2 = router.clone();
    step_route(msg, router).and_then(move |link_actor: Recipient<Event>| {
        step_handle_event(msg2, link_actor).and_then(move |event_response: EventResponse| {
            step_handle_directive_inner(router2, event_response)})
    })
}



struct CommuterApi;

impl CommuterApi {

    fn enum_routes((qparams, state): (Query<messages::EnumRoutes>, web::Data<AppState>)) -> impl Future<Item = HttpResponse, Error = Error> {
        // TO-DO: use .filter in messages::EnumRoutes
        // let msg: messages::EnumRoutes = qparams;
        state.router
            .send(qparams.into_inner())
            .from_err()
            .and_then(
                |res| Ok(HttpResponse::Ok().json(res))
            )
    }

    fn get_route((route, state): (Path<String>, web::Data<AppState>)) -> impl Future<Item = HttpResponse, Error = Error> {
        let msg = messages::GetRoute{ route: route.to_string() };
        state.router
            .send(msg)
            .from_err()
            .and_then(|res| match res {
                    Some(r) => Ok(HttpResponse::Ok().json(r)),
                    None => Ok(HttpResponse::NotFound().content_type("text/plain").body("No such route")),
            })

    }

    fn create_route((route, conf, query, state): (Path<String>, Json<models::LinkConf>, Query<WriteModeQuery>, web::Data<AppState>)) -> impl Future<Item = HttpResponse, Error = Error> {
        let msg = messages::CreateRoute {
            route: route.to_string(),
            conf: conf.into_inner(),
            force: query.force
        };
        state.router
            .send(msg)
            .from_err()
            .and_then(|ok| {
                if ok {
                    Ok(HttpResponse::Ok().json(OperationResponse::default()))
                } else {
                    Ok(actix_web::HttpResponse::Conflict().content_type("text/plain").body("Resource already exists"))
                }
            })
    }

    fn update_route((route, conf, state): (Path<String>, Json<models::LinkConf>, web::Data<AppState>)) -> impl Future<Item = HttpResponse, Error = Error> {
        // TO-OD: Check existence of the route, return 404 if not found
        let msg = messages::UpdateRoute {
            route: route.to_string(),
            conf: conf.into_inner(),
        };
        state.router
            .send(msg)
            .from_err()
            .and_then(|ok| {
                if ok {
                    Ok(HttpResponse::Ok().json(OperationResponse::default()))
                } else {
                    Ok(actix_web::HttpResponse::NotFound().content_type("text/plain").body("Not found"))
                }
            })
    }

    fn delete_route((route, state): (Path<String>, web::Data<AppState>)) -> impl Future<Item = HttpResponse, Error = Error> {
        // TO-OD: Check existence of the route, return 404 if not found
        let msg = messages::DropRoute {
            route: route.to_string(),
        };
        state.router
            .send(msg)
            .from_err()
            .and_then(|ok| {
                if ok {
                    Ok(HttpResponse::Ok().json(OperationResponse::default()))
                } else {
                    Ok(actix_web::HttpResponse::NotFound().content_type("text/plain").body("Not found"))
                }
            })
    }

    #[inline]
    fn handle_signal(route: String, query: HashMap<String, String>, headers: HeadersUniqMap, router: Addr<dpcore::Router>) -> impl Future<Item = ResponderModel, Error = SvcError> {
        let event = encode_event(route, query, headers).expect("Error encode_event");
        handle_event_inner(event, router.clone())
    }

    fn handle_signal_get((route, query, headers, state): (Path<String>, Query<HashMap<String,String>>, Headers, web::Data<AppState>)) -> impl Future<Item = HttpResponse, Error = Error> {
        // let event = encode_event(route.into_inner(), query.into_inner(), headers.into_inner()).expect("Error encode_event");
        // handle_event(event, state.router.clone()).from_err()
        CommuterApi::handle_signal(route.into_inner(), query.into_inner(), headers.into_inner(), state.router.clone()).and_then(|r|{
                fut_ok(HttpResponse::Ok().content_type(r.content_type.clone()).body(r.content.clone()))
        }).from_err()
    }

    fn handle_signal_get_bulk((route, query, headers, state): (Path<String>, Query<HashMap<String,String>>, Headers, web::Data<AppState>)) -> impl Future<Item = HttpResponse, Error = Error> {
        // Split query to vector, spawn many futures, union them.,
        // See solutions: https://docs.rs/futures/0.1.25/futures/future/fn.join_all.html; And: https://stackoverflow.com/a/49089303
        // Extract list of ids: 
        let headers = headers.into_inner();
        let route = route.into_inner();
        let router = state.router.clone();
        let mut query = query.into_inner();
        // let ids_value = query.remove("_in").expect("List of items missed!");
        // println!("--->route {}", route.clone());
        let ids: Vec<String> = query.remove("_in").expect("List of items missed!").split(" ").map(|s|s.to_string()).collect();
        // println!("--->ids {:?}", ids.clone());
        join_all(
            ids.into_iter().map(move |id|{

                let subroute = if route.len() == 0 {
                    id
                } else {
                    format!("{}/{}", route, id)
                };
                
                // println!("-> subroute {}", subroute.clone());

                CommuterApi::handle_signal(subroute, query.clone(), headers.clone(), router.clone())
            })
        ).then(|res|{
            let htresp = match res {
                Ok(list) => {
                    let responces = list.into_iter().map(|r| r.content).collect::<Vec<String>>();
                    let serialized = format!("[{}]", itertools::join(responces, ","));
                    let content_type = "application/json";
                    HttpResponse::Ok().content_type(content_type).body(serialized)
                },
                Err(e) => e.error_response() // Uses ResponseError trait implementation from errors.rs module
            };
            fut_ok(htresp)
        }).map_err(|e: SvcError|Error::from(e))
    }

}

#[derive(Clone, Deserialize, Debug, Default)]
struct WriteModeQuery {
    #[serde(default)]
    force: Option<bool>
}

// fn build_lua_message(route: String, query: HashMap<String, String>, headers: HeadersUniqMap) -> LuaMessage {
//     let query: HashMap<String, LuaMessage> = query.iter().map(|(k, v)| (k.to_string(), LuaMessage::from(v.to_string()))).collect();
//     let headers: HashMap<String, LuaMessage> = headers.iter().map(|(k, v)| (k.to_string(), LuaMessage::from(v.to_string()))).collect();
//     let table: HashMap<String, LuaMessage> = HashMap::new();

//     table.insert("path".to_string(), LuaMessage::from(route));
//     table.insert("query".to_string(), LuaMessage::from(query));
//     table.insert("headers".to_string(), LuaMessage::from(headers));

//     LuaMessage::from(table)
// }

// fn reduce_route(script: String, route: String, query: HashMap<String, String>, headers: HeadersUniqMap, state: web::Data<AppState>) -> impl Future<Item = Option<String>, Error = Error> {
//     let msg = build_lua_message(route, query, headers);
//     let lua = LuaActorBuilder::new()
//         .on_handle_with_lua(&script)
//         .build()
//         .unwrap().start();

//     Box::new(
//         lua
//             .send(msg)
//             .from_err()
//             .and_then(|res| {
//                 match res {
//                     LuaMessage::String(s) => fut_ok(Some(s)),
//                     LuaMessage::Nil => fut_ok(None),
//                     // ignore everything else
//                     _ => unimplemented!(),
//                 }
//             })
//     )

// }

// fn get_route_payload(route: String, query: HashMap<String, String>, headers: HeadersUniqMap, state: AppState) -> FutureResponse<Option<models::LinkConf>> {
//     let msg = messages::GetRoute {
//         route: route.clone()
//     }
//     Box::new(
//         state.router
//             .send(msg)
//             .from_err()
//             .and_then(|resp| {
//                 match resp {
//                     None => fut_ok(None),
//                     Some(route_response) => {
//                             match route_response.payload {
//                                 Mediator(mediator) => {
//                                     reduce_route(mediator.lua_script, route, query, headers, state)
//                                         .from_err()
//                                         .and_then(|route| {
//                                             match route {
//                                                 None => fut_ok(None),
//                                                 Some(route) => get_route_payload(route, query, state)
//                                             }
//                                         })
//                                 },
//                                 Data(data) => fut_ok(Some(DataEntry(data))),
//                             }                                
//                         }
//                 }
//             })
//     )
// }

// CONF API (END)




#[get("/robots.txt")]
fn robots_txt() -> &'static str {
    "User-agent: *\nDisallow: /\r\n"
}

// Snippet for actix_web 1.x: 

// #[get("/resource1/{name}/index.html")]
// fn index(req: HttpRequest, name: web::Path<String>) -> String {
//     println!("REQ: {:?}", req);
//     format!("Hello: {}!\r\n", name)
// }



struct AppState {
    pub router: Addr<Router>,
}


use std::sync::mpsc;
use std::thread;

fn main() -> std::io::Result<()> {
    dotenv().ok();

    
    ::std::env::set_var("RUST_LOG", "error,actix_web=error,main=error");
    env_logger::init();

    // let port = env::args().nth(1).ok_or("Port parameter is invalid or missed!".to_owned()).and_then(|arg| arg.parse::<u32>().map_err(|err| err.to_string())).unwrap();
    let port = match env::args().nth(1) {
        Some(arg) => arg.parse::<u32>().map_err(|err| err.to_string()).unwrap(),
        _ => 8010,
    };

    let srv_addr = format!("0.0.0.0:{}", port);

    let X_COOKIE_HEADER: &'static str = "x-cookie";


    // Routes payload

    // fn load_config() -> Result<Config, ConfError> {
    //     // Try to load from postgres:
    //     if let Ok(_) = env::var("SNOWBALL_CONF_FROM_DB") {
    //         return PostgresProvider::load_config("")
    //     }
    //     if let Ok(c) = TomlProvider::load_config("server.toml"){
    //         println!("TOML config found");
    //         return Ok(c)
    //     }
    //     if let Ok(c) = EnvProvider::load_config("SNOWBALL_ROUTES"){
    //         println!("Env config found");
    //         return Ok(c)
    //     }
    //     let msg = String::from("No config sources available!");
    //     Err(ConfError::TypeError(msg))
    // }

    // let payload = match load_config(){
    //     Ok(c) => c,
    //     Err(e) => {
    //         panic!("Cannot load config! {}", e)
    //     }
    // };

    // let routes = payload.enum_routes();

    // println!("Serialized: {}", serde_json::to_string(&payload).unwrap());


    let (tx, rx) = mpsc::channel();

    let server_thread = thread::spawn(move || {

        let sys = actix::System::new("http2buff");
        println!("Starting http server: {}", &srv_addr);

        let registry_addr = RoutesRegistry::from_registry();


        // TO_DO: SyncContext: Context.set_mailbox_capacity() up from 16!
        HttpServer::new(move || {
            
            let shared_data = web::Data::new(AppState{
                // router: Arbiter::start(move|_| Router::new())
                router: Router::new().start()
                // buffers
            });
            
            App::new().register_data(shared_data.clone())
                .wrap(
                    // Cors::default()
                    Cors::new()
                        // .allowed_origin("*") // <- Avoid credentials in client!!! See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Origin
                        .allowed_methods(vec![
                            "HEAD",
                            "OPTIONS",
                            "GET", 
                            "POST",
                            "DELETE"
                            ])
                        .allowed_headers(vec![
                            http::header::ACCEPT,
                            http::header::CONTENT_TYPE,
                            http::header::CONTENT_LENGTH,
                            // http::header::USER_AGENT,
                            http::header::ORIGIN, 
                            // http::header::AUTHORIZATION, 
                            http::header::COOKIE, 
                            http::header::HeaderName::from_static(X_COOKIE_HEADER),
                            ])
                        .supports_credentials()
                        .max_age(3600)
                )
                // enable logger
                .wrap(middleware::Logger::default())
                .service(web::resource("/solve/{tail:.*}")
                    .route(web::get().to_async(CommuterApi::handle_signal_get)))
                .service(web::resource("/bulk/{tail:.*}")
                    .route(web::get().to_async(CommuterApi::handle_signal_get_bulk)))
                .service(web::resource("/conf/route")
                    .route(web::get().to_async(CommuterApi::enum_routes)))
                .service(web::resource("/conf/route/{tail:.*}")
                    .route(web::get().to_async(CommuterApi::get_route))
                    .route(web::post().to_async(CommuterApi::create_route))
                    .route(web::patch().to_async(CommuterApi::update_route))
                    .route(web::delete().to_async(CommuterApi::delete_route)))
                .service(robots_txt)
        }).bind(srv_addr)
            .unwrap()
            .keep_alive(5)
            // .workers(4)
            .shutdown_timeout(1)
            .start();
        
        let _ = tx.send(registry_addr).expect("Cannot send registry address to channel");            
        let _ = sys.run();


    });

    let registry_addr = rx.recv().unwrap();
    let _ = registry_addr.send(messages::Load).wait().expect("RoutesRegistry error on load");

    server_thread.join().expect("Error in the main thread");

    Ok(())

}


// ---------------- *< ----------------------


// See nice idea - move some code to 'appconfig.rs':

// use actix_web::{error, web};

// use crate::handlers::{parts, products};

// pub fn config_app(cfg: &mut web::ServiceConfig) {
//     // domain includes: /products/{product_id}/parts/{part_id}
//     cfg.service(
//         web::scope("/products")
//             .service(
//                 web::resource("")
//                     .route(web::get().to_async(products::get_products))
//                     .route(web::post().to_async(products::add_product)),
//             )
//             .service(
//                 web::scope("/{product_id}")
//                     .service(
//                         web::resource("")
//                             .route(web::get().to_async(products::get_product_detail))
//                             .route(web::delete().to_async(products::remove_product)),
//                     )
//                     .service(
//                         web::scope("/parts")
//                             .service(
//                                 web::resource("")
//                                     .route(web::get().to_async(parts::get_parts))
//                                     .route(web::post().to_async(parts::add_part)),
//                             )
//                             .service(
//                                 web::resource("/{part_id}")
//                                     .route(web::get().to_async(parts::get_part_detail))
//                                     .route(web::delete().to_async(parts::remove_part)),
//                             ),
//                     ),
//             ),
//     );
// }
// ---------------

// And use it in main.rs: 

// use actix_web::{middleware, App, HttpServer};

// use async_ex2::appconfig::config_app;

// fn main() -> std::io::Result<()> {
//     std::env::set_var("RUST_LOG", "actix_server=info,actix_web=info");
//     env_logger::init();

//     HttpServer::new(|| {
//         App::new()
//             .configure(config_app)
//             .wrap(middleware::Logger::default())
//     })
//     .bind("127.0.0.1:8080")?
//     .run()
// }


// ---

// Details: https://github.com/actix/examples/blob/master/async_ex2/src/bin/main.rs