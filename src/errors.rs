use actix::{MailboxError};
use serde_json::error::{Error as SerdeJsonError};
use actix_web::{
    error,
    Error as AWError, 
    ResponseError,
    HttpResponse,
};
use derive_more::Display; // naming it clearly for illustration purposes

use actix_lua::dev::rlua::Error as LuaError;

#[derive(Fail, Debug, Display)]
pub enum SvcError {
    #[display(fmt = "Not Found")]
    NotFound,    
    #[display(fmt = "Too many requests")]
    Overload,
    #[display(fmt = "Json error")]
    SerdeJson,
    #[display(fmt = "Recursive Routing")]
    RecurrentRouting,
    #[display(fmt = "Lua Error")]
    LuaError(String),
    #[display(fmt = "Unknown Error")]
    UnknownError(String),
}

/// Actix web uses `ResponseError` for conversion of errors to a response
impl ResponseError for SvcError {
    fn error_response(&self) -> HttpResponse {
        match self {
            SvcError::NotFound => {
                println!("do some stuff related to NotFound error");
                HttpResponse::NotFound().finish()
            }

            SvcError::Overload => {
                println!("Overload error");
                HttpResponse::BadRequest().finish()
            }

            SvcError::RecurrentRouting => {
                println!("Recursive routing");
                HttpResponse::NotFound().finish()
            }

            SvcError::SerdeJson => {
                println!("Json processing error");
                HttpResponse::BadRequest().finish()
            }

            SvcError::LuaError(s) => {
                println!("Scripting error");
                HttpResponse::BadRequest().body(s)
            }

            _ => {
                println!("Unknown error");
                HttpResponse::InternalServerError().finish()
            }
        }
    }
}

impl From<MailboxError> for SvcError {
    fn from(actix_error: MailboxError) -> Self {
        SvcError::Overload {}
    }
}

impl From<SerdeJsonError> for SvcError {
    fn from(serde_error: SerdeJsonError) -> Self {
        SvcError::SerdeJson {}
    }
}

impl From<LuaError> for SvcError {
    fn from(lua_error: LuaError) -> Self {
        SvcError::LuaError(format!("{:?}", lua_error))
    }
}

impl From<error::Error> for SvcError {
    fn from(std_error: error::Error) -> Self {
        SvcError::UnknownError(format!("{}", std_error))
    }
}


// // errors.rs
// use std::fmt;

// use actix_web::{error::ResponseError, HttpResponse};


// #[derive(Fail, Debug)]
// pub enum ServiceError {
//     #[fail(display = "Internal Server Error")]
//     InternalServerError,

//     #[fail(display = "Overflow Error")]
//     OverflowError,

//     #[fail(display = "BadRequest: {}", _0)]
//     BadRequest(String),

//     #[fail(display = "Storage Error: {}", _0)]
//     StorageError(String),

//     #[fail(display = "Processing Error: {}", _0)]
//     ProcessingError(String),
// }

// // impl ResponseError trait allows to convert our errors into http responses with appropriate data
// impl ResponseError for ServiceError {
//     fn error_response(&self) -> HttpResponse {
//         match *self {
//             ServiceError::InternalServerError => {
//                 HttpResponse::InternalServerError().json("Internal Server Error")
//             },
//             ServiceError::OverflowError => {
//                 HttpResponse::InternalServerError().json("Overflow Error")
//             },
//             ServiceError::BadRequest(ref message) => HttpResponse::BadRequest().json(message),
//             ServiceError::StorageError(ref message) => HttpResponse::InternalServerError().json(message),
//             ServiceError::ProcessingError(ref message) => HttpResponse::InternalServerError().json(message),
//         }
//     }
// }

// #[derive(Fail, Debug)]
// pub enum ConfError {
//     IoError(std::io::Error),
//     TypeError(String),
// }

// impl fmt::Display for ConfError {
//     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//         match self {
//             ConfError::IoError(ref e) => e.fmt(f),
//             // This is a wrapper, so defer to the underlying types' implementation of `fmt`.
//             ConfError::TypeError(s) => write!(f, "{}", s),
//             _ => write!(f, "{}", "unknown"),
//         }
//     }
// }

// impl From<std::io::Error> for ConfError {
//     fn from(err: std::io::Error) -> ConfError {
//         ConfError::IoError(err)
//     }
// }

// // Low-level storage Errors

// #[derive(Fail, Debug)]
// pub enum WriteError {
//     IoError(std::io::Error),
//     DB(postgres::Error),
//     Redis(redis::RedisError),
//     EncodeError(String),
// }

// // impl WriteErrorTrait for WriteError {}

// impl fmt::Display for WriteError {
//     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//         match self {
//             WriteError::IoError(ref e) => e.fmt(f),
//             // This is a wrapper, so defer to the underlying types' implementation of `fmt`.
//             WriteError::DB(ref e) => e.fmt(f),
//             WriteError::Redis(ref e) => e.fmt(f),
//             WriteError::EncodeError(s) => write!(f, "{}", s),
//             _ => write!(f, "{}", "unknown"),
//         }
//     }
// }

// // This will be automatically called by `?` if a `ParseIntError`
// // needs to be converted into a `WriteError`.
// impl From<std::io::Error> for WriteError {
//     fn from(err: std::io::Error) -> WriteError {
//         WriteError::IoError(err)
//     }
// }

// // This will be automatically called by `?` if a `ParseIntError`
// // needs to be converted into a `WriteError`.
// impl From<postgres::Error> for WriteError {
//     fn from(err: postgres::Error) -> WriteError {
//         WriteError::DB(err)
//     }
// }

// // This will be automatically called by `?` if a `ParseIntError`
// // needs to be converted into a `WriteError`.
// impl From<redis::RedisError> for WriteError {
//     fn from(err: redis::RedisError) -> WriteError {
//         WriteError::Redis(err)
//     }
// }

// impl From<WriteError> for ServiceError {
//     fn from(err: WriteError) -> ServiceError {
//         match err {
//             WriteError::IoError(ref e) => ServiceError::StorageError(format!("IoError: {:?}", e)),
//             // This is a wrapper, so defer to the underlying types' implementation of `fmt`.
//             WriteError::DB(ref e) => ServiceError::StorageError(format!("DB error (Postgres): {:?}", e)),
//             WriteError::Redis(ref e)  => ServiceError::StorageError(format!("Redis Error: {:?}", e)),

//             WriteError::EncodeError(ref e)  => ServiceError::BadRequest(format!("Encode Error: {:?}", e)),
//         }
//     }
// }


// // impl From<actix::prelude::SendError<relay::transport::Send>> for WriteError {}

// #[derive(Fail, Debug)]
// pub enum MessageError {
//     Generic(String),
//     RouteNotFound(String),
//     HandlerNotImplemented(String),
// }

// impl From<MessageError> for ServiceError {
//     fn from(err: MessageError) -> ServiceError {
//         match err {
//             MessageError::Generic(s) => ServiceError::ProcessingError(s),
//             MessageError::RouteNotFound(s) => ServiceError::ProcessingError(s),
//             MessageError::HandlerNotImplemented(s) => ServiceError::ProcessingError(s),
//         }
//     }
// }

// impl fmt::Display for MessageError {
//     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//         match self {
//             MessageError::Generic(s) => write!(f, "{}", s),
//             _ => write!(f, "{}", "unknown"),
//             MessageError::RouteNotFound(s) => write!(f, "{}", s),
//             _ => write!(f, "{}", "unknown"),
//             MessageError::HandlerNotImplemented(s) => write!(f, "{}", s),
//             _ => write!(f, "{}", "unknown"),
//         }
//     }
// }
