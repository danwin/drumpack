#!/bin/bash
# Many thanks to: https://blog.semicolonsoftware.de/building-minimal-docker-containers-for-rust-applications/
docker build . -t drumpack-builder
docker run --rm -it -v "$(pwd)/dist":/home/rust/dist drumpack-builder /bin/bash